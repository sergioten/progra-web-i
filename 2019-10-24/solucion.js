const events = require("events");
const net = require("net");

const isStringEmpty = s => s.trim() === "";

const splitArrayBy = predicate => xs => {
  const chunks = [[]];
  let activeChunk = 0;

  xs.forEach(x => {
    const isSplitting = predicate(x);

    if (isSplitting) {
      chunks.push([]);
      activeChunk++;
      return;
    }

    chunks[activeChunk].push(x);
  });

  return chunks;
};
const splitArrayByEmptyLine = splitArrayBy(isStringEmpty);

const toPairs = obj => {
  const keys = Object.keys(obj);
  return keys.map(key => ({ key, value: obj[key] }));
};

const fromPairs = pairs =>
  pairs.reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});

const createServer = () => {
  const requestEmitter = new events.EventEmitter();

  let socket;

  const send = (status, headers, body) => {
    socket.write(`HTTP/1.1 ${status}\r\n`);
    toPairs(headers).forEach(({ key, value }) =>
      socket.write(`${key}: ${value}\r\n`)
    );
    socket.write("\r\n");
    socket.write(body ? body + "\r\n\r\n\r\n" : "\r\n");
    socket.end();
  };

  const server = net.createServer(_socket => {
    console.log("received connection");

    if (socket) throw new Error("more than one connection is not allowed");
    socket = _socket;
    socket.on("end", () => {
      socket = undefined;
      console.log("connection closed");
    });

    let buffer = "";
    socket.on("data", data => {
      buffer += data.toString("utf8");

      const isRequestFinished = /\r\n\r\n\r\n$/.test(buffer);
      if (!isRequestFinished) return;

      const [start, ...headersAndBody] = buffer.split("\r\n");
      const [method, path, protocol] = start.split(" ").filter(w => w !== "");
      const [headerLines, bodyLines] = splitArrayByEmptyLine(headersAndBody);
      const headers = fromPairs(
        headerLines.map(line => line.trim().split(/:\s*/))
      );
      const body = bodyLines.join("\r\n");

      /* Implementación alternativa con RegExp.
      const matches = buffer.match(
        /^ *([^ ]+) *([^ ]+) *([^ ]+)\r\n((?:(?<!\r\n\r\n)[^])+)\r\n(?:\r\n([^]+)\r\n)?\r\n\r\n$/
      );

      const [, method, path, protocol, headerLines, body] = matches;
      console.log("matches", matches);
      */

      requestEmitter.emit(
        "request",
        { method, path, protocol, headers, body },
        { send }
      );
    });
  });

  return {
    on: requestEmitter.on.bind(requestEmitter),
    listen: server.listen.bind(server)
  };
};

module.exports = createServer;
